package com.keith.mybatis.sqlsession;

import java.sql.SQLException;
import java.util.List;
/**ToDo:sqlsession主要是为了存储通用会话接口
 *
 * @author wangkui
 */
public interface Sqlsession {
    /**ToDo:单个查询接口
     *
     * @author wangkui
     * @param statementId 需要执行的mapper的id
     * @param param 需要传入的参数
     * @return T
     */
    <T> T selectOne(String statementId, Object param);

    /**ToDo:多个查询接口
     *
     * @author wangkui
     * @param statementId 需要执行的mapper的id
     * @param param 需要传入的参数
     * @return List<T>
     */
    <T> List<T> selectList(String statementId, Object param);
}
