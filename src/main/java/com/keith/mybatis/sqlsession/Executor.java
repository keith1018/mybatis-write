package com.keith.mybatis.sqlsession;


import com.keith.mybatis.config.Configration;
import com.keith.mybatis.config.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public interface Executor {
    <T> List<T> query(Configration configuration, MappedStatement mappedStatement, Object param);
}
