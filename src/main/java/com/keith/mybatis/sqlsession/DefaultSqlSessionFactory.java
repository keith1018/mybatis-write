package com.keith.mybatis.sqlsession;


import com.keith.mybatis.config.Configration;


/**
 * TODO:
 *
 * @author wangkui
 * @date 2020/5/22 10:12
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory{
    private Configration configration;

    public DefaultSqlSessionFactory(Configration configration) {
        this.configration = configration;
    }
    @Override
    public Sqlsession openSession(){
        return new DefaultSqlsession(configration);
    }
}
