package com.keith.mybatis.sqlsession;

import com.keith.mybatis.config.Configration;
import com.keith.mybatis.config.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public class DefaultSqlsession implements Sqlsession{
    private Configration configration;

    public DefaultSqlsession(Configration configration) {
        this.configration = configration;
    }

    @Override
    public <T> T selectOne(String statementId, Object param){
        List<T> list = selectList(statementId,param);
        if(list != null && list.size() == 1){
            return list.get(0);
        }else{
            throw new RuntimeException("查询结果为空或者查询结果大于1！");
        }

    }

    @Override
    public <T> List<T> selectList(String statementId, Object param) {
        /** 构建执行器*/
        Executor executor = new SimpleExecutor();
        /** 拿到需执行的statement*/
        MappedStatement mappedStatement = configration.getMappedStatementMap().get(statementId);
        List<T> list = executor.query(configration,mappedStatement,param);
        return list;
    }
}
