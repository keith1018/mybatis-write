package com.keith.mybatis.sqlsession;

import com.keith.mybatis.config.Configration;
import com.keith.mybatis.config.XmlConfigParser;
import com.keith.mybatis.sqlsession.DocumentReader;
import org.dom4j.Document;

import java.io.InputStream;
import java.io.Reader;

/**
 * TODO:SqlSessionFactory构建类
 *
 * @author wangkui
 * @date 2020/5/22 11:18
 */
public class SqlSessionFactoryBuilder {
    private Configration configration;

    public SqlSessionFactoryBuilder() {
        this.configration = new Configration();
    }
    /** 字节流构造方法*/
    public SqlSessionFactory build(InputStream inputStream){
        Document document = DocumentReader.createDocument(inputStream);
        XmlConfigParser xmlConfigParser = new XmlConfigParser(configration);
        xmlConfigParser.parserConfif(document);
        return build();
    }
    /** 字符流构造方法*/
    public SqlSessionFactory build(Reader reader){
        /** 待补充*/
        return build();
    }
    /** 复用构造方法*/
    private SqlSessionFactory build(){
        return new DefaultSqlSessionFactory(configration);
    }

}
