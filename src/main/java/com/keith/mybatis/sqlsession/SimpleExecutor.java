package com.keith.mybatis.sqlsession;

import com.keith.mybatis.config.BoundSql;
import com.keith.mybatis.config.Configration;
import com.keith.mybatis.config.MappedStatement;
import com.keith.mybatis.config.ParameterMapping;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * TODO:
 *
 * @author wangkui
 * @date 2020/5/25 11:23
 */
public class SimpleExecutor implements Executor {
    @Override
    public <T> List<T> query(Configration configration, MappedStatement mappedStatement, Object param) {
        /** 1获取数据源和连接*/
        Connection connection = getConnection(configration);

        /** 2执行sql*/
        List<Object> resultList = executeSql(connection,mappedStatement,param);

        return (List<T>)resultList;
    }
    /**ToDo:执行sql
     *
     * @author wangkui
     * @param
     * @return java.sql.ResultSet
     */
    private List<Object> executeSql(Connection connection,MappedStatement mappedStatement, Object param) {

        List<Object> resultList = new ArrayList<>();
        BoundSql boundSql = mappedStatement.getSqlSource().getBoundSql();
        String sql = boundSql.getSql();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        Class paramType = mappedStatement.getParamType();
        Class resultType = mappedStatement.getResultType();
        String statement = mappedStatement.getStatementType();
        if("prepared".equals(statement)){
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                /** 判断参数类型是否是基础数据类型，此处但处理integer，其余类型可以此类推*/
                if (paramType == Integer.class) {
                    preparedStatement.setInt(1, (Integer) param);
                } else {
                    /** 如果不是基础类型，那可能是对象，list或者map，此处单举例对象*/
                    /** 1先循环所有需要的参数集合*/
                    for (int i = 0; i < parameterMappings.size(); i++) {
                        /** 拿到第一个参数*/
                        ParameterMapping parameterMapping = parameterMappings.get(i);
                        String name = parameterMapping.getName();
                        // 通过反射获取入参对象中执行名称的属性值
                        Field filed = paramType.getDeclaredField(name);
                        // 设置暴力赋值，不管属性是不是私有
                        filed.setAccessible(true);
                        Object o = filed.get(param);
                        preparedStatement.setObject(i + 1, o);
                    }
                }
                ResultSet resultSet = preparedStatement.executeQuery();
                /** 将数据放入到对象返回*/
                while (resultSet.next()) {
                    /** 获取数据对象*/
                    ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
                    /** 获取返回值对象*/
                    Object obj = resultType.newInstance();
                    int count = resultSetMetaData.getColumnCount();
                    for (int i = 1; i <= count; i++) {
                        /** 获取数据对象的列名*/
                        String name = resultSetMetaData.getColumnName(i);
                        Field field = resultType.getDeclaredField(name);
                        field.setAccessible(true);
                        field.set(obj, resultSet.getObject(name));
                    }
                    resultList.add(obj);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return resultList;
    }

    /**ToDo:获取数据源
     *
     * @author wangkui
     * @param configration
     * @return java.sql.Connection
     */
    private Connection getConnection(Configration configration){
        DataSource dataSource = configration.getDataSource();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }


}
