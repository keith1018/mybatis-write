package com.keith.mybatis.sqlsession;

/**
 * TODO:
 *
 * @author wangkui
 * @date 2020/5/22 10:12
 */
public interface SqlSessionFactory {
    /**ToDo:openSession底层就是做各种成员变量的初始化;例如：configuration，executor，dirty(内存当中的数据与数据库中)
     *
     * @author wangkui
     * @param
     * @return com.keith.mybatis.sqlsession.Sqlsession
     */
    Sqlsession openSession();
}
