package com.keith.mybatis.config;

import com.keith.mybatis.sqlsession.DocumentReader;
import org.apache.commons.dbcp.BasicDataSource;
import org.dom4j.Document;
import org.dom4j.Element;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * TODO:
 *
 * @author wangkui
 * @date 2020/5/22 11:28
 */
public class XmlConfigParser {
    private Configration configration;

    public XmlConfigParser(Configration configration) {
        this.configration = configration;
    }

    public void parserConfif(Document document){
        Element element = document.getRootElement();
        //解析mybatis配置
        parseEnvironments(element.element("environments"));
        //解析mapper配置文件
        parserMappers(element.element("mappers"));
    }

    private void parserMappers(Element mappers) {
        /** 获取到配置文件中的所有mapper标签*/
        List<Element> mapperList = mappers.elements("mapper");
        for (Element element:mapperList){
            parserMapper(element);
        }
    }

    private void parserMapper(Element element) {
        String resource = element.attributeValue("resource");
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resource);
        Document document = DocumentReader.createDocument(inputStream);
        XmlMapperParser xmlMapperParser = new XmlMapperParser(configration);
        xmlMapperParser.parserMapper(document);
    }
    /**ToDo:解析环境（此处配置信息很多，处理也较多，但是意思都是一样的，这边只简单处理数据源 ）
     *
     * @author wangkui
     * @param environments
     * @return void
     */
    private void parseEnvironments(Element environments) {
        Element environment = environments.element("environment");
        Element dataSource = environment.element("dataSource");
        parserDatasource(dataSource);
    }
    /**ToDo:解析数据源
     *
     * @author wangkui
     * @param dataSource
     * @return void
     */
    private void parserDatasource(Element dataSource) {
        List<Element> propertys = dataSource.elements();
        BasicDataSource ds = new BasicDataSource();
        /** 存放数据库配置*/
        Properties properties = new Properties();
        propertys.stream().forEach(property -> {
            String name = property.attributeValue("name");
            String value = property.attributeValue("value");
            properties.setProperty(name,value);
        });
        ds.setDriverClassName(properties.getProperty("driver"));
        ds.setUrl(properties.getProperty("url"));
        ds.setUsername(properties.getProperty("username"));
        ds.setPassword(properties.getProperty("password"));
        configration.setDataSource(ds);
    }

}
