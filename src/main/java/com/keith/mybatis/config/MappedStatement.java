package com.keith.mybatis.config;

/**
 * TODO:用于存储mapper文件中的mapper对象
 *
 * @author wangkui
 * @date 2020/5/22 10:35
 */
public class MappedStatement {
    /** mapper的id*/
    private String id;
    /** mapper参数类型*/
    private Class<?> paramType;
    /** mapper返回值类型*/
    private Class<?> resultType;
    /** mapper的sql语句（未解析版本）*/
    private SqlSource sqlSource;
    /** mapper类型*/
    private String statementType;

    public MappedStatement(String id, Class<?> paramType, Class<?> resultType, SqlSource sqlSource, String statementType) {
        this.id = id;
        this.paramType = paramType;
        this.resultType = resultType;
        this.sqlSource = sqlSource;
        this.statementType = statementType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class<?> getParamType() {
        return paramType;
    }

    public void setParamType(Class<?> paramType) {
        this.paramType = paramType;
    }

    public Class<?> getResultType() {
        return resultType;
    }

    public void setResultType(Class<?> resultType) {
        this.resultType = resultType;
    }

    public SqlSource getSqlSource() {
        return sqlSource;
    }

    public void setSqlSource(SqlSource sqlSource) {
        this.sqlSource = sqlSource;
    }

    public String getStatementType() {
        return statementType;
    }

    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }
}
