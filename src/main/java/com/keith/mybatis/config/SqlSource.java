package com.keith.mybatis.config;

import com.keith.mybatis.utils.GenericTokenParser;
import com.keith.mybatis.utils.ParameterMappingTokenHandler;

/**
 * TODO:原sql类
 *
 * @author wangkui
 * @date 2020/5/22 10:44
 */
public class SqlSource {
    private String sqlText;

    public SqlSource(String sqlText) {
        this.sqlText = sqlText;
    }
    /**ToDo:获取解析版本sql
     *
     * @author wangkui
     * @param
     * @return com.keith.mybatis.config.BoundSql
     */
    public BoundSql getBoundSql(){
        /** 运用解析工具，匹配#{}，将整体替换为？，返回解析完成后的sql*/
        ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler();
        GenericTokenParser  tokenParser = new GenericTokenParser("#{","}",handler);
        String sql = tokenParser.parse(this.sqlText);
        /** 返回解析后的sql和参数的对象*/
        return new BoundSql(sql,handler.getParameterMappings());
    }
}
