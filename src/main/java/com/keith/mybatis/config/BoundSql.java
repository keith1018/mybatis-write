package com.keith.mybatis.config;


import java.util.ArrayList;
import java.util.List;

/**
 * TODO:解析版本的sql
 *
 * @author wangkui
 * @date 2020/5/22 10:49
 */
public class BoundSql {
    /** 解析过的sql*/
    private String sql;
    /** 解析出来的参数*/
    private List<ParameterMapping> parameterMappings = new ArrayList<>();

    public BoundSql(String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void addParameterMapping(ParameterMapping parameterMapping) {
        this.parameterMappings.add(parameterMapping);
    }
}
