package com.keith.mybatis.config;

import org.dom4j.Document;
import org.dom4j.Element;

import java.util.List;

/**
 * TODO:
 *
 * @author wangkui
 * @date 2020/5/22 16:05
 */
public class XmlMapperParser {
    private Configration configration;

    private String namespace;

    public XmlMapperParser(Configration configration) {
        this.configration = configration;
    }
    /**ToDo:解析mapper配置文件
     *
     * @author wangkui
     * @param document
     * @return void
     */
    public void parserMapper(Document document) {
        Element rootElement = document.getRootElement();
        this.namespace = rootElement.attributeValue("namespace");
        List<Element> elements = rootElement.elements("select");
        parseStatements(elements);
    }
    /**ToDo:循环解析mapper文件中的mapper标签内容
     *
     * @author wangkui
     * @param elements
     * @return void
     */
    private void parseStatements(List<Element> elements) {
        elements.stream().forEach(select->{
            parseStatement(select);
        });
    }
    /**ToDo:解析具体的mapper标签成mappedStatement对象
     * 
     * @author wangkui
     * @param select
     * @return void
     */
    private void parseStatement(Element select) {
        /** 解析*/
        String id = namespace +"."+select.attributeValue("id");
        Class parameterType = getClassType(select.attributeValue("parameterType"));
        Class resultType = getClassType(select.attributeValue("resultType"));
        String statementType = select.attributeValue("statementType");
        String sqlText = select.getTextTrim();
        SqlSource sqlSource = new SqlSource(sqlText);
        MappedStatement mappedStatement = new MappedStatement(id,parameterType,resultType,sqlSource,statementType);
        /** 存放到配置文件中*/
        configration.addMappedStatementMap(id,mappedStatement);
    }
    /**ToDo:获取class
     *
     * @author wangkui
     * @param parameterType
     * @return java.lang.Class<?>
     */
    private Class<?> getClassType(String parameterType) {
        if (parameterType == null || parameterType.equals("")) {
            return null;
        }
        try {
            Class<?> clazz = Class.forName(parameterType);
            return clazz;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
