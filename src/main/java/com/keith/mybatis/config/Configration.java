package com.keith.mybatis.config;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: 简易版mybatis配置文件 所有的配置信息和mapper内容都存放再此处
 *
 * @author wangkui
 * @date 2020/5/22 10:34
 */
public class Configration {
    /** 数据库配置信息*/
    private DataSource dataSource;
    /** mapper内容存放再此*/
    private Map<String,MappedStatement> mappedStatementMap = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMappedStatementMap() {
        return mappedStatementMap;
    }

    public void addMappedStatementMap(String id,MappedStatement mappedStatement) {
        this.mappedStatementMap.put(id,mappedStatement);
    }
}
