package com.keith.mybatis.dao;

import com.keith.mybatis.po.User;

public interface UserDao {

	User queryUserById(User u);
}
