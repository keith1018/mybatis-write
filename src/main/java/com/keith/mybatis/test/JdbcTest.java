package com.keith.mybatis.test;

import java.sql.*;

/**
 * TODO:
 *
 * @author wangkui
 * @date 2020/4/19 10:36
 */
public class JdbcTest {

    public static void main(String[] args) {
        Connection connection = getConnection();
        execute(connection);
    }

    //获取jdbc连接
    public static Connection getConnection() {
        Connection connection = null;
        try {
            /** 加载驱动*/
            Class.forName("com.mysql.cj.jdbc.Driver");
            /** 设置链接、用户名、密码*/
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test?serverTimezone=Asia/Shanghai", "root", "123456");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    //执行sql
    public static void execute(Connection connection) {
        String sql = "select * from users where id = ?";
        PreparedStatement st;
        try {
            /** 关闭自动提交，即开始事务*/
            connection.setAutoCommit(false);
            /** 设置sql*/
            st = connection.prepareStatement(sql);
            /** 塞入参数*/
            st.setString(1, "1");
            /** 执行*/
            ResultSet rs = st.executeQuery();
            /** 处理查询结果*/
            while (rs.next()) {
                System.out.println(rs.getString("id") + "---" + rs.getString("username"));
            }
            /** 提交事务*/
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                /** 将连接恢复到初始状态*/
                connection.setAutoCommit(true);
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}
